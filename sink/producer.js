const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const KAFKA_IP = 'kafka:9092';
// Each table in your module will have a Kafka producer. 
// TOPIC_NAME_USER defines the topic name to be used by the user producer.
const TOPIC_NAME_USER = 'user';

var Kafka = require('node-rdkafka');

// Creating producer
var user_producer = new Kafka.Producer({
  'metadata.broker.list': KAFKA_IP,
  'dr_cb': true  //delivery report callback
});
var user_producer_is_ready = false;

//logging debug messages, if debug is enabled
user_producer.on('event.log', function(log) {
  console.log(log);
});

//logging all errors
user_producer.on('event.error', function(err) {
  console.error('Error from producer');
  console.error(err);
});

//logs the delivery reports
user_producer.on('delivery-report', function(err, report) {
  console.log('delivery-report: ' + JSON.stringify(report));
});

//polls kafka to check if the messages are being delivered
user_producer.on('ready', function(arg) {
  // user_producer_is_ready = true;
  console.log('producer ready.' + JSON.stringify(arg));
  //need to keep polling for a while to ensure the delivery reports are received
  var pollLoop = setInterval(function() {
    user_producer.poll();
  }, 1000);
});

//disconnects the producer
user_producer.on('disconnected', function(arg) {
  console.log('producer disconnected. ' + JSON.stringify(arg));
});

// var user = {
//   login: "admin",
//   email: "admin@hacknizer.com",
//   password: "1234",
//   biography: "I'm an admin",
//   avatar: "google.com",
//   test: ""
// };

// for(i = 0; i <100; i++) {
//   user.test = i;
//   send_value(JSON.stringify(user), user_producer);
// }

//starting the producer
user_producer.connect();

// This variable represents an entity of User, the table in your module related to this producer
var user = {
  login: "admin",
  email: "admin@hacknizer.com",
  password: "1234",
  biography: "I'm an admin",
  avatar: "google.com",
  test: ""
};

rl.on('line', (input) => {
  user.test = input;
  // send_value(JSON.stringify(user), user_producer);
  user_producer.produce(TOPIC_NAME_USER, -1, new Buffer(JSON.stringify(user)))
});

// This function sends a message to Kafka, but only after the producer is ready to do so.
function send_value(value, producer) {
  createProducerReadyPromise(producer)
    .then(function (producer) {
      // while (!user_producer_is_ready) {
      // }
      producer.produce(TOPIC_NAME_USER, -1, new Buffer(value));
    });
}

// This function is used to prevent the producer from trying to send a message before it's ready.
function createProducerReadyPromise(producer) {
  return new Promise(function (resolve, reject) {
    producer.on('ready', function () {
      resolve(producer);
    });
  });
}